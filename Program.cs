﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LR1_OPI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A*x^4 + B*x^2 + C = 0\nВведiть значення АВС");
            double a, b, c, x1, x2, d;
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());
            if(a!=0)
            {
                d = b * b - 4 * a * c;
                if (d < 0)
                {
                    Console.WriteLine("Коренiв немає");
                }
                else if (d == 0)
                {
                    x1 = (b*(-1) / (2 * a));
                    if(x1>0)
                    {
                        Console.WriteLine("1й корiнь рiвняння = {0}\n2й корiнь рiвняння = {0}", Math.Sqrt(x1).ToString(), (-Math.Sqrt(x1)).ToString());
                    }
                    if (x1 == 0)
                    {
                        Console.WriteLine("Корiнь рiвняння = {0}", x1.ToString());
                    }
                    if(x1<0)
                    {
                        Console.WriteLine("Коренiв немає");
                    }
                    //Console.WriteLine("Корiнь рiвняння = {0}",x1.ToString());
                }
                else if (d > 0)
                {
                    int count = 0;
                    x1 = ((b * (-1) + Math.Sqrt(d)) / (2 * a));
                    x2 = ((b * (-1) - Math.Sqrt(d)) / (2 * a));
                    if (x1 > 0)
                    {
                        Console.WriteLine("{0}й корiнь рiвняння = {1}\n{2}й корiнь рiвняння = {3}",count+1, Math.Sqrt(x1).ToString(),count+2, (-Math.Sqrt(x1)).ToString());
                        count += 2;
                    }
                    else if (x1 == 0)
                    {
                        Console.WriteLine("{0}й корiнь рiвняння = {1}", count + 1, x1.ToString());
                        count++;
                    }
                    if (x2 > 0)
                    {
                        Console.WriteLine("{0}й корiнь рiвняння = {1}\n{2}й корiнь рiвняння = {3}", count + 1, Math.Sqrt(x2).ToString(), count + 2, (-Math.Sqrt(x2)).ToString());
                        count += 2;
                    }
                    else if (x2 == 0)
                    {
                        Console.WriteLine("{0}й корiнь рiвняння = {1}", count + 1, x2.ToString());
                        count ++;
                    }
                    if(x1<0 && x2<0) 
                    {
                        Console.WriteLine("Коренiв немає");
                    }
                }
            }
            else
            {
                Console.WriteLine("A не може дорiвнювати 0");
            }
            Console.ReadLine();
        }
    }
}
